// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
//	UE_LOG(LogTemp, Warning, TEXT("Interact!"));

	//UE_LOG(LogTemp, Warning, TEXT("The boolean value inside AFOOD::Interact is %s"), (bIsHead ? TEXT("true") : TEXT("false")));
	if (bIsHead == true) {

		UE_LOG(LogTemp, Warning, TEXT("Inside if!"));
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
		}
		int x, y;
		int xstart = -490;
		int xend = 81;
		int ystart = -980;
		int yend = 860;
		x = rand() % (xend - xstart + 1) + xstart;
		y = rand() % (yend - ystart + 1) + ystart;
		FVector NewLocation(x,y,0);
		this->SetActorLocation(NewLocation);
	}
}

