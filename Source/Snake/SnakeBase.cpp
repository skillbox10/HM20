// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
//	GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	UE_LOG(LogTemp, Warning, TEXT("Hello"));
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
//	SnakeElements.Num()*ElementSize;
	for (int i = 0; i < ElementsNum; i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("The integer value is: %d"), SnakeElements.Num());
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		int32 FuckUIndex = SnakeElements.FindLast(0);

		if (ElementsNum == 1) {
			bool isIt = false;
			if (IsValid(SnakeElements.Last())) {
				
				FVector NewLocation(SnakeElements.Last()->GetActorLocation());
				FTransform NewTransform(NewLocation);
				ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
				NewSnakeElem->SnakeOwner = this;
				int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
				if (ElemIndex == 0)
				{
					NewSnakeElem->SetFirstElementType();

				}
			}
		}
		else {
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();

			}
		}
		

	}
}

void ASnakeBase::Move() 
{
	FVector MovementVector(ForceInitToZero);

//	MovementVector = FVector(MovementSpeedDelta, 0, 0);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:

		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElem = SnakeElements[i];
		auto PrevElem = SnakeElements[i - 1];
		FVector PrevLocation = PrevElem->GetActorLocation();
		CurrentElem->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other) {
	if (IsValid(OverlappedElement))
	{

		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		UE_LOG(LogTemp, Warning, TEXT("The boolean value inside ASnakeBase is %s"), (bIsFirst ? TEXT("true") : TEXT("false")));
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

